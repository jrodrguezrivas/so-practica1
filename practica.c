#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <math.h>
#include <time.h>

int main ()
{

        pid_t pid;
        int x;
        int *keys;
        int statuses[4];
        key_t key;
        key_t key2;
        int  shmid, status,shmid2;
        struct shmid_ds buf;
        int *vector;
       	int jm;
       


        key = ftok("practica.c", 1);
        if ((shmid = shmget(key, 100*sizeof(int), IPC_CREAT | 0777)) == -1)
                exit(1);
        vector = (int *) shmat(shmid, NULL, 0);
        if ((shmid2 = shmget(key2, 4*sizeof(int), IPC_CREAT | 0777)) == -1)
                exit(1);
        keys = (int *) shmat(shmid2, NULL, 0);
        

        srand(time(NULL)); 
 
        for(x=1;x<=3;x++)
        {
  
          pid=fork(); 
          if(pid>0)
          {
          		   keys[x]=pid;
                   printf("Soy el proceso %d\n",getpid());
                   for (int j = 0; j < 100; ++j)
						vector[j]=rand() % 101;;
                   			
                   
                   
          }

          else if (pid==0)
          {        
                   if (x==1)
                   {

                   		 int indice,valor;
                   		//HIJO 1
                      printf("----------------------Proceso de Hijo 1 empezado----------------------\n");
                   		for (int i = 0; i < 10; ++i)
                   		{
                   			printf("Introduzca el indice\n");
	                   		scanf(" %d",&indice);
	                   		printf("Introduzca el valor\n");
	                   		scanf(" %d",&valor);
	                   		vector[indice]=valor;
                   		}
                      printf("----------------------Proceso de Hijo 1 acabado----------------------\n");
                   
                      exit(1);
                   		
                   		
                    }

                   
                   
                   else if(x==2)
                   {

                   		 int indice,valor;

                   		//HIJO 2
                   		printf("----------------------Proceso de Hijo 2 empezado----------------------\n");
                   		for (int i = 0; i < 100; ++i)
                   		{
                   			indice=rand() % 10;
                   			valor=rand() % 101;

                   			vector[indice]=valor;
                   			printf("Quedan %d cambios por hacer\n",100-i );
            				    sleep(1);
                   		}

                      printf("----------------------Proceso de Hijo 2 acabado----------------------\n");

                      exit(1);
                   		

                   }

                   
                   
                   else if (x==3)
                   {
                   		
                   		//HIJO 3
                   		
                   		
                   		int suma;
                   		printf("----------------------Proceso de Hijo 3 empezado----------------------\n");
                   		for (int i = 0; i < 5; ++i)
                   		{
                   			suma=0;
                   			for (int j = 0; j < 100; ++j)
                   			{
                   				suma=suma+vector[j];
                   			}
                   			printf("Quedan  %d sumas\n",5-i);
                   			sleep(1);
                   		}

                   		printf("----------------------Proceso de Hijo 3 acabado----------------------\n");
                      exit(1);

	
                   }

                	

                   
          }
        }


        //Esperar hijo
        waitpid(keys[1], NULL, 0);
       	waitpid(keys[2], NULL, 0);
       	waitpid(keys[3], NULL, 0);
        shmdt(vector); //Limpia directamente.
        shmdt(keys); //Limpia directamente.
        shmctl(shmid, IPC_RMID, &buf); //Cuando los procesos se acaben, limpia memoria.
        shmctl(shmid2, IPC_RMID, &buf); //Cuando los procesos se acaben, limpia memoria.
        return 0;
  
 }

        


      